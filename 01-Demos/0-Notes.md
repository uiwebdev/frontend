
## html basic

white spaces, breaks tag, headings h1 to h6 , p tags, ul and ol, ol with type A, a, 1 etc..

Forms - text, pswd, phno, text area, color, date, time etc


---
adding styles to html page with link tag

expand list style - on hover 

```css
 ul ul {
      visibility: hidden;
      height: 0;
    }

    li:hover > ul {
      visibility: visible;
      height: auto;
    }
```


### 3d button

```css
 button {
    margin: 50px;
    background-color: tomato;
    color: wheat;
    padding: 10px;
    font-size: 1.5rem;
    
    box-shadow: grey 8px 8px 5px;
    transition: .3s;
}

button:hover {
    transform: translate(-10px, -10px);
    box-shadow: grey 18px 18px 10px;
}

button:active {
    transform: translate(5px, 5px);
    box-shadow: grey 3px 3px 1px;
}
```
